App = (() => {
  // const endpoint = "http://localhost:8080/api/v1";
  const endpoint = "https://backend-task-jwt.blockpanthers.tk/api/v1";
  return {
    login: (email_id, password) => {
      let input_params = {
        email_id,
        password,
      };
      $.ajax({
        url: endpoint + "/users/signin",
        method: "POST",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(input_params),
        crossDomain: true,
        processData: false,
        success: function (data) {
          sessionStorage.setItem("AccessToken", data.authToken);
          sessionStorage.setItem("email_id", email_id);
          window.location.href = "/main_menu";
        },
        error: function (jqXhr, textStatus, errorThrown) {
          if (errorThrown == "Forbidden") {
            toastr["warning"]("Credential did NOT match");
            window.location.href = "/";
          } else {
            toastr[jqXhr["responseJSON"]["type"] || "error"](
              jqXhr["responseJSON"]["message"]
            );
          }
        },
      });
    },
    getMoviesList: () => {
      $("#movies").DataTable({
        // searching: false,
        destroy: true,
        processing: true,
        serverSide: true,
        // scrollX: true,
        ordering: false,

        ajax: {
          type: "POST",
          headers: {
            Authorization: App.getToken(),
          },
          url: "/api/v1/movies/get/all",
          error: function (xhr, error, code) {
            $("#movies").DataTable().destroy();
            $("#movies").DataTable({ searching: false, scrollX: true });

            if (xhr.status == 403) {
              toastr.error(xhr.responseJSON.message, "Warning");
              window.location.href = "/";
            } else {
              toastr.error(xhr.responseJSON.message, "Error");
            }
          },
        },
        columns: [
          { data: "movie_name" },
          { data: "rating" },
          { data: "cast" },
          { data: "genre" },
          { data: "release_date" },
        ],
      });
    },
    addMovie: () => {
      let input_params = {
        movie_name: $("#movie_name").val(),
        rating: $("#rating").val(),
        cast: $("#cast").val(),
        genre: $("#genre").val(),
        release_date: $("#release_date").val(),
      };
      $.ajax({
        url: endpoint + "/movies/insert",
        method: "POST",
        headers: {
          Authorization: App.getToken(),
        },
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(input_params),
        crossDomain: true,
        processData: false,
        success: (data) => {
          $("#movie_name").val("");
          $("#rating").val("");
          $("#cast")
            .val("")
            .select2({
              multiple: true,
              tags: true,
              createTag: function (tag) {
                return { id: tag.term, text: tag.term, tag: true };
              },
            });
          $("#genre").val("");
          $("#release_date").val("");

          toastr["success"](data["message"]);
        },
        error: (jqXhr, textStatus, errorThrown) => {
          if (jqXhr["status"] == 403) {
            toastr["warning"](jqXhr["responseJSON"]["message"]);
            window.location.href = "/";
          } else {
            toastr["error"](jqXhr["responseJSON"]["message"]);
          }
        },
      });
    },
    getMoviesName: () => {
      $.ajax({
        url: endpoint + "/movies/get/all/names",
        method: "GET",
        headers: {
          Authorization: App.getToken(),
        },
        dataType: "json",
        contentType: "application/json",
        crossDomain: true,
        processData: false,
        success: ({ data = [] }) => {
          // data = data?.data?.rows?.map((d) => ({
          //   id: d?.id,
          //   text: d?.movie_name,
          // }));
          $("#searchResult").empty();

          $("#searchResult").select2({
            placeholder: "Select your movie",
            allowClear: true,
            data,
          });

          $("#searchResult").val("").trigger("change");

          // toastr["success"](data["message"]);
        },
        error: (jqXhr, textStatus, errorThrown) => {
          if (jqXhr["status"] == 403) {
            toastr["warning"](jqXhr["responseJSON"]["message"]);
            window.location.href = "/";
          } else {
            toastr["error"](jqXhr["responseJSON"]["message"]);
          }
        },
      });
    },
    searchMovie: () => {
      $.ajax({
        url: endpoint + "/movies/get?movie_id=" + $("#searchResult").val(),
        method: "GET",
        headers: {
          Authorization: App.getToken(),
        },
        dataType: "json",
        contentType: "application/json",
        crossDomain: true,
        processData: false,
        success: ({ data = {} }) => {
          $("#movie_name").val(data?.movie_name);
          $("#rating").val(data?.rating);
          $("#cast")
            .select2({ multiple: true, tags: true, tags: data?.cast })
            .val(data?.cast)
            .trigger("change");
          $("#genre").val(data?.genre);
          $("#release_date").val(data?.release_date);

          if (window.location.pathname == "/delete_movie") {
            $("input").attr("readonly", "true");
            $("select").attr("disabled", "true");
          } else {
            $("input").removeAttr("readonly");
            $("select").removeAttr("disabled");
          }

          // $("#searchResult").attr("disabled", "true");
        },
        error: (jqXhr, textStatus, errorThrown) => {
          if (jqXhr["status"] == 403) {
            toastr["warning"](jqXhr["responseJSON"]["message"]);
            window.location.href = "/";
          } else {
            toastr["error"](jqXhr["responseJSON"]["message"]);
          }
        },
      });
    },
    editMovie: () => {
      let param = {
        movie_id: $("#searchResult").val(),
        movie_name: $("#movie_name").val(),
        cast: $("#cast").val(),
        genre: $("#genre").val(),
        rating: $("#rating").val(),
        release_date: $("#release_date").val(),
      };
      $.ajax({
        url: endpoint + "/movies/update",
        method: "POST",
        headers: {
          Authorization: App.getToken(),
        },
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(param),
        crossDomain: true,
        processData: false,
        success: (data) => {
          toastr["success"](data?.message);

          App.getMoviesName();
        },
        error: (jqXhr, textStatus, errorThrown) => {
          if (jqXhr["status"] == 403) {
            toastr["warning"](jqXhr["responseJSON"]["message"]);
            window.location.href = "/";
          } else {
            toastr["error"](jqXhr["responseJSON"]["message"]);
          }
        },
      });
    },
    deleteMovie: () => {
      let param = {
        movie_id: $("#searchResult").val(),
      };
      $.ajax({
        url: endpoint + "/movies/delete",
        method: "POST",
        headers: {
          Authorization: App.getToken(),
        },
        dataType: "json",
        data: JSON.stringify(param),
        contentType: "application/json",
        crossDomain: true,
        processData: false,
        success: (data) => {
          toastr["success"](data?.message);

          App.getMoviesName();
        },
        error: (jqXhr, textStatus, errorThrown) => {
          if (jqXhr["status"] == 403) {
            toastr["warning"](jqXhr["responseJSON"]["message"]);
            window.location.href = "/";
          } else {
            toastr["error"](jqXhr["responseJSON"]["message"]);
          }
        },
      });
    },
    signup: () => {
      let param = {
        first_name: $("#first_name").val(),
        mobile: $("#mobile").val(),
        email_id: $("#email_id").val(),
        password: $("#password").val(),
      };
      $.ajax({
        url: endpoint + "/users/signup",
        method: "POST",
        dataType: "json",
        data: JSON.stringify(param),
        contentType: "application/json",
        crossDomain: true,
        processData: false,
        success: function (data) {
          toastr["success"]("Logged out successfully");
          window.location.href = "/";
        },
        error: function (jqXhr, textStatus, errorThrown) {
          if (errorThrown == "Forbidden") {
            toastr["warning"]("Credential did NOT match");
          } else {
            toastr["error"](jqXhr["responseJSON"]["message"]);
          }
        },
      });
    },
    checkToken: () => {
      if (
        sessionStorage.AccessToken != "" &&
        sessionStorage.AccessToken != null
      ) {
        return true;
      } else {
        App.logout();
      }
    },
    logout: () => {
      sessionStorage.clear();
      window.location.href = "/";
    },
    getToken: () => {
      return "Bearer " + sessionStorage.getItem("AccessToken");
    },
  };
})();

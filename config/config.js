require("dotenv").config();

module.exports = {
  development: {
    username: process.env.LOCAL_DB_USER,
    password: process.env.LOCAL_DB_SECRET,
    database: process.env.LOCAL_DB_NAME,
    host: process.env.LOCAL_DB_HOST,
    port: process.env.LOCAL_DB_PORT,
    logging: false,
    dialect: "postgres",
  },
  staging: {
    username: process.env.STAGE_DB_USER,
    password: process.env.STAGE_DB_SECRET,
    database: process.env.STAGE_DB_NAME,
    host: process.env.STAGE_DB_HOST,
    port: process.env.STAGE_DB_PORT,
    logging: false,
    dialect: "postgres",
  },
  production: {
    username: process.env.LIVE_DB_USER,
    password: process.env.LIVE_DB_SECRET,
    database: process.env.LIVE_DB_NAME,
    host: process.env.LIVE_DB_HOST,
    port: process.env.LIVE_DB_PORT,
    logging: false,
    dialect: "postgres",
  },
};

"use strict";

const bcrypt = require("bcrypt");

const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class users extends Model {
    static associate(models) {}
  }
  users.init(
    {
      id: {
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
      },
      mobile: {
        type: DataTypes.STRING,
      },
      email_id: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      password: {
        type: DataTypes.STRING,
      },
      is_active: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
    },
    {
      sequelize,
      modelName: "users",
      schema: "auth",
      underscored: true,
    }
  );

  users.beforeCreate((user, options) => {
    if (user?.password && user?.email_id) {
      user.password = bcrypt.hashSync(user.password, bcrypt.genSaltSync(10));
      user.email_id = user?.email_id ? user?.email_id.toLowerCase() : "";
    }

    // if (user?.email_id) {
    //   user.email_id = user?.email_id ? user?.email_id.toLowerCase() : "";
    // }
  });

  return users;
};

"use strict";

const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class user_profiles extends Model {
    static associate(models) {
      // Assocations
      user_profiles.belongsTo(models.users, {
        foreignKey: "user_id",
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      });

      user_profiles.belongsTo(models.user_profiles, {
        foreignKey: "created_by",
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      });
      user_profiles.belongsTo(models.user_profiles, {
        foreignKey: "updated_by",
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      });
    }
  }
  user_profiles.init(
    {
      id: {
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
      },
      first_name: {
        type: DataTypes.STRING,
      },
      last_name: {
        type: DataTypes.STRING,
      },
      gender: {
        type: DataTypes.ENUM("Male", "Female", "Others"),
      },
      dob: {
        type: DataTypes.DATEONLY,
      },
      age: {
        type: DataTypes.INTEGER,
      },
      is_active: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      created_at: {
        type: DataTypes.DATE,
        defaultValue: new Date().toISOString(),
      },
      updated_at: {
        type: DataTypes.DATE,
        defaultValue: new Date().toISOString(),
      },
    },
    {
      sequelize,
      modelName: "user_profiles",
      underscored: true,
    }
  );

  return user_profiles;
};

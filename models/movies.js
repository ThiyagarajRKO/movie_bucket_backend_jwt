"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class movies extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      movies.belongsTo(models.user_profiles, {
        foreignKey: "created_by",
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      });
      movies.belongsTo(models.user_profiles, {
        foreignKey: "updated_by",
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      });
    }
  }
  movies.init(
    {
      id: {
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
      },
      movie_name: {
        type: DataTypes.STRING,
      },
      rating: {
        type: DataTypes.INTEGER,
      },
      cast: {
        type: DataTypes.ARRAY(DataTypes.STRING),
      },
      genre: {
        type: DataTypes.STRING,
      },
      release_date: {
        type: DataTypes.DATEONLY,
      },
      is_active: {
        type: DataTypes.BOOLEAN,
      },
      created_at: {
        type: DataTypes.DATE,
        defaultValue: new Date().toISOString(),
      },
      updated_at: {
        type: DataTypes.DATE,
        defaultValue: new Date().toISOString(),
      },
    },
    {
      sequelize,
      modelName: "movies",
      underscored: true,
    }
  );
  return movies;
};

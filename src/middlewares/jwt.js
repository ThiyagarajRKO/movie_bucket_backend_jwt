import jwt from "jsonwebtoken";

export const generateToken = (payload) => {
  return new Promise((resolve, reject) => {
    try {
      let token = jwt.sign(payload, process.env.JWT_TOKEN_KEY, {
        expiresIn: "1d",
      });
      resolve(token);
    } catch (error) {
      console.error(error);
      reject(error);
    }
  });
};

export const verifyToken = (token) => {
  return new Promise((resolve, reject) => {
    try {
      let payload = jwt.verify(token, process.env.JWT_TOKEN_KEY);
      resolve(payload);
    } catch (error) {
      reject(error);
    }
  });
};

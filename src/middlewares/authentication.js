import { UserProfiles } from "../controllers";
import { verifyToken } from "./jwt";

export const ValidateJWT = async (req, res, next) => {
  try {
    if (!req.headers.authorization) {
      return res.status(403).send({ message: "Unauthorized User!" });
    }

    let payload = await verifyToken(req.headers.authorization.split(" ")[1]);

    if (!payload?.pid) {
      return res.status(403).send({
        type: "Warning",
        message: "This is NOT a valid access token!",
      });
    }

    let userCount = await UserProfiles.CountById({ id: payload?.pid });

    if (userCount == 0) {
      return res.status(403).send({ message: "Invalid User!" });
    }

    req.tokenProfileId = payload.pid;

    next();
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

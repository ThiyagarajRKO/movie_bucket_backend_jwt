import { Users, UserProfiles } from "../../controllers";
import bcrypt from "bcrypt";
import { generateToken } from "../../middlewares/jwt";

export const SignIn = ({ email_id, password }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let result = await Users.GetByEmail({
        email_id,
      });

      if (!result) {
        return reject({ message: "User does NOT exist!" });
      }

      if (!bcrypt.compareSync(password, result?.password)) {
        return reject({ message: "Password does NOT match!" });
      }

      // Getting user profile`
      let user_profile = await UserProfiles.GetByUserId({
        user_id: result?.id,
      });

      if (!user_profile) {
        return reject({ message: "User Profile does NOT exist!" });
      }

      let token = await generateToken({
        pid: user_profile?.id,
      });

      resolve({
        message: "Access granted!",
        profileId: user_profile?.id,
        authToken: token,
      });
    } catch (err) {
      reject(err);
    }
  });
};

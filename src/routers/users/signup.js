import { Users, UserProfiles } from "../../controllers";

export const SignUp = ({ first_name, email_id, mobile, password }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let userCount = await Users.Count({ email_id });
      if (userCount > 0) {
        return reject({
          statusCode: 409,
          type: "warning",
          message: "Email already exists!",
        });
      }

      // User Creation
      let result = await Users.Insert({
        first_name,
        email_id,
        mobile,
        password,
      });

      // User Profile creation
      await UserProfiles.Insert({
        user_id: result?.id,
        first_name,
      });

      resolve({
        message: "Account has been created successfully!",
        user_id: result?.id,
      });
    } catch (err) {
      reject(err);
    }
  });
};

import express from "express";
import { body, validationResult } from "express-validator";
import { SignUp } from "./signup";
import { SignIn } from "./signin";
import { UserInfo } from "./user_info";
import { ValidateJWT } from "../../middlewares/authentication";

const userRouters = express.Router();

userRouters.post(
  "/signup",
  body("email_id").isEmail().withMessage("It's NOT a valid email"),
  body("password")
    .isLength(8)
    .withMessage("Password size must be greater than or equal to 8"),
  body("first_name").notEmpty().withMessage("First Name must NOT be empty"),
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({
          type: "warning",
          message: "Invalid inputs!",
          errors: errors.array(),
        });
      }

      let result = await SignUp(req.body);
      res.status(200).send({ type: "success", ...result });
    } catch (err) {
      res
        .status(err?.statusCode || 400)
        .send({ type: err?.type || "error", message: err?.message || err });
    }
  }
);

userRouters.post(
  "/signin",
  body("email_id").isEmail().withMessage("It's NOT a valid email"),
  body("password")
    .isLength(8)
    .withMessage("Password size must be greater than or equal to 8"),
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({
          type: "warning",
          message: "Invalid inputs!",
          errors: errors.array(),
        });
      }

      let result = await SignIn(req.body, req.pid);
      res.status(200).send({ type: "success", ...result });
    } catch (err) {
      res
        .status(err?.statusCode || 400)
        .send({ type: err?.type || "error", message: err?.message || err });
    }
  }
);

userRouters.get("/info", ValidateJWT, async (req, res) => {
  try {
    let result = await UserInfo(req.tokenProfileId);
    res.status(200).send({ type: "success", ...result });
  } catch (err) {
    res
      .status(err?.statusCode || 400)
      .send({ type: err?.type || "error", message: err?.message || err });
  }
});

export default userRouters;

import { UserProfiles } from "../../controllers";

export const UserInfo = (tokenProfileId) => {
  return new Promise(async (resolve, reject) => {
    try {
      // Getting user profile`
      let data = await UserProfiles.Get({
        id: tokenProfileId,
      });

      if (!data) {
        return reject({ message: "User Profile does NOT exist!" });
      }

      resolve({
        data,
      });
    } catch (err) {
      reject(err);
    }
  });
};

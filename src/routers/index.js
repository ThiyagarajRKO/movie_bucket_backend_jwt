import express from "express";
import userRouters from "./users";
import moviesRouters from "./movies";
import morgan from "morgan";
import { ValidateJWT } from "../middlewares/authentication";

const router = express.Router();
morgan("combined");

// Public Routers
router.use("/users", userRouters);

router.use(ValidateJWT);
// Private Routers
router.use("/movies", moviesRouters);

export default router;

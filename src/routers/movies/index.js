import express from "express";
import { body, validationResult } from "express-validator";
import { Insert, Delete, Get, GetAll, Update, GetAllNames } from "./movies";

const moviesRouters = express.Router();

moviesRouters.post(
  "/insert",
  body("movie_name").notEmpty().withMessage("Movie Name must NOT be empty"),
  body("rating").notEmpty().isNumeric().withMessage("Invalid Rating"),
  body("cast").isArray().withMessage("Cast must be an array"),
  body("genre").notEmpty().withMessage("Genre must be an array"),
  body("release_date").notEmpty().withMessage("Release Date must NOT be empty"),
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res
          .status(400)
          .json({ message: "Invalid inputs!", errors: errors.array() });
      }

      let params = {
        profile_id: req?.tokenProfileId,
        ...req.body,
      };
      let result = await Insert(params);
      res.status(200).send({ type: "success", ...result });
    } catch (err) {
      res
        .status(err?.statusCode || 400)
        .send({ type: err?.type || "error", message: err?.message || err });
    }
  }
);

moviesRouters.post(
  "/update",
  body("movie_id").notEmpty().withMessage("ID must NOT be empty"),
  body("movie_name").notEmpty().withMessage("Movie Name must NOT be empty"),
  body("rating").notEmpty().isNumeric().withMessage("Invalid Rating"),
  body("cast").isArray().withMessage("Cast must be an array"),
  body("genre").notEmpty().withMessage("Genre must be an array"),
  body("release_date").notEmpty().withMessage("Release Date must NOT be empty"),
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res
          .status(400)
          .json({ message: "Invalid inputs!", errors: errors.array() });
      }

      let params = {
        profile_id: req?.tokenProfileId,
        ...req.body,
      };
      let result = await Update(params);
      res.status(200).send({ type: "success", ...result });
    } catch (err) {
      res
        .status(err?.statusCode || 400)
        .send({ type: err?.type || "error", message: err?.message || err });
    }
  }
);

moviesRouters.post(
  "/delete",
  body("movie_id").notEmpty().withMessage("ID must NOT be empty"),
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res
          .status(400)
          .json({ message: "Invalid inputs!", errors: errors.array() });
      }

      let params = {
        profile_id: req?.tokenProfileId,
        ...req.body,
      };
      let result = await Delete(params);
      res.status(200).send({ type: "success", ...result });
    } catch (err) {
      res
        .status(err?.statusCode || 400)
        .send({ type: err?.type || "error", message: err?.message || err });
    }
  }
);

moviesRouters.post("/get/all", async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res
        .status(400)
        .json({ message: "Invalid inputs!", errors: errors.array() });
    }

    let params = {
      profile_id: req?.tokenProfileId,
      ...req.body,
    };

    let result = await GetAll(params);
    res.status(200).send({ type: "success", ...result });
  } catch (err) {
    res
      .status(err?.statusCode || 400)
      .send({ type: err?.type || "error", message: err?.message || err });
  }
});

moviesRouters.get("/get/all/names", async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res
        .status(400)
        .json({ message: "Invalid inputs!", errors: errors.array() });
    }

    let params = {
      profile_id: req?.tokenProfileId,
      ...req.query,
    };

    let result = await GetAllNames(params);
    res.status(200).send({ type: "success", ...result });
  } catch (err) {
    res
      .status(err?.statusCode || 400)
      .send({ type: err?.type || "error", message: err?.message || err });
  }
});

moviesRouters.get("/get", async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res
        .status(400)
        .json({ message: "Invalid inputs!", errors: errors.array() });
    }

    let params = {
      profile_id: req?.tokenProfileId,
      ...req.query,
    };

    let result = await Get(params);
    res.status(200).send({ type: "success", ...result });
  } catch (err) {
    res
      .status(err?.statusCode || 400)
      .send({ type: err?.type || "error", message: err?.message || err });
  }
});

export default moviesRouters;

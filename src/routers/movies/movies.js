import { Movies } from "../../controllers";

export const Insert = ({
  profile_id,
  movie_name,
  rating,
  cast,
  genre,
  release_date,
}) => {
  return new Promise(async (resolve, reject) => {
    try {
      let movieCount = await Movies.CountByMovieName({
        profile_id,
        movie_name,
      });

      if (movieCount > 0) {
        return reject({ message: "Movie Name already exists!" });
      }

      let result = await Movies.Insert({
        profile_id,
        movie_name,
        rating,
        cast,
        genre,
        release_date,
      });

      resolve({
        message: "Movie details has been inserted successfully!",
        movie_id: result?.id,
      });
    } catch (err) {
      reject(err);
    }
  });
};

export const Update = ({
  movie_id,
  profile_id,
  movie_name,
  rating,
  cast,
  genre,
  release_date,
}) => {
  return new Promise(async (resolve, reject) => {
    try {
      let result = await Movies.Update({
        id: movie_id,
        profile_id,
        movie_name,
        rating,
        cast,
        genre,
        release_date,
      });

      if (result?.[0] == 0) {
        return reject({
          type: "warning",
          message: "Please check the movie id",
        });
      }
      resolve({
        message: "Movie details has been updated successfully!",
        movie_id: result?.id,
      });
    } catch (err) {
      reject(err);
    }
  });
};

export const Delete = ({ profile_id, movie_id }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let result = await Movies.Delete({
        id: movie_id,
        profile_id,
      });

      resolve({
        message: "Movie details has been deleted successfully!",
        movie_id: result?.id,
      });
    } catch (err) {
      reject(err);
    }
  });
};

export const Get = (params) => {
  return new Promise(async (resolve, reject) => {
    try {
      let data = await Movies.Get(params);

      resolve({ data });
    } catch (err) {
      reject(err);
    }
  });
};

export const GetAll = (params) => {
  return new Promise(async (resolve, reject) => {
    try {
      let result = await Movies.GetAll(params);

      resolve({
        data: result?.rows,
        iTotalRecords: result?.rows?.length,
        iTotalDisplayRecords: result?.count,
      });
    } catch (err) {
      reject(err);
    }
  });
};

export const GetAllNames = (params) => {
  return new Promise(async (resolve, reject) => {
    try {
      let data = await Movies.GetAllNames(params);

      resolve({
        data,
      });
    } catch (err) {
      reject(err);
    }
  });
};

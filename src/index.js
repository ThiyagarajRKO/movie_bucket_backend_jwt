import express from "express";
import routers from "./routers";
import dotenv from "dotenv";
import cookieParser from "cookie-parser";
import rateLimit from "express-rate-limit";
import cors from "cors";

dotenv.config();
const app = express();

// Security
app.disable("x-powered-by");

const limiter = rateLimit({
  windowMs: 1000, // 15 minutes
  max: 8, // limit each IP to 100 requests per windowMs
  message: {
    type: "Warning",
    message: "You've reached your limit! Please try in sometime.",
  },
});

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());

// app.use(
//   sessions({
//     secret: process.env.SESSION_SECRET,
//     saveUninitialized: false,
//     cookie: { maxAge: 1000 * 60 * 60 * 24 }, // One Day
//     resave: false,
//   })
// );

app.use(cookieParser());

app.use(express.static("public"));
app.set("view engine", "ejs");

app.get("/", (req, res) => {
  res.render("index");
});

app.get("/main_menu", (req, res) => {
  res.render("main_menu");
});

app.get("/new_movie", (req, res) => {
  res.render("new_movie");
});

app.get("/view_movies", (req, res) => {
  res.render("view_movies");
});

app.get("/edit_movie", (req, res) => {
  res.render("edit_movie");
});

app.get("/delete_movie", (req, res) => {
  res.render("delete_movie");
});

app.get("/signup", (req, res) => {
  res.render("signup");
});

app.use(limiter);

app.use("/api/v1", routers);

app.listen(process.env.PORT, console.log("Server is running..."));

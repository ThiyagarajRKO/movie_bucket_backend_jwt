import models from "../../models";
import bcrypt from "bcrypt";
const Op = models.Sequelize.Op;

export const Insert = ({ email_id, mobile, password }) => {
  return new Promise(async (resolve, reject) => {
    try {
      // password = bcrypt.hashSync(password, bcrypt.genSaltSync(10));

      let result = await models.users.create({
        email_id,
        mobile,
        password,
        is_active: true,
      });

      resolve(result);
    } catch (err) {
      reject(err);
    }
  });
};

export const Count = ({ email_id }) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (!email_id) {
        return reject({ message: "Email Id must NOT be empty!" });
      }

      let result = await models.users.count({
        where: {
          [Op.and]: models.sequelize.where(
            models.sequelize.fn("lower", models.sequelize.col("email_id")),
            email_id?.toLowerCase()
          ),
          is_active: true,
        },
        raw: true,
      });
      resolve(result);
    } catch (err) {
      reject(err);
    }
  });
};

export const GetByEmail = ({ email_id }) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (!email_id) {
        return reject({ message: "Email Id must NOT be empty!" });
      }

      let result = await models.users.findOne({
        where: {
          [Op.and]: models.sequelize.where(
            models.sequelize.fn("lower", models.sequelize.col("email_id")),
            email_id?.toLowerCase()
          ),
          is_active: true,
        },
        raw: true,
      });
      resolve(result);
    } catch (err) {
      reject(err);
    }
  });
};

import models from "../../models";
const Op = models.Sequelize.Op;

export const Insert = ({
  profile_id,
  movie_name,
  rating,
  cast,
  genre,
  release_date,
}) => {
  return new Promise(async (resolve, reject) => {
    try {
      // password = bcrypt.hashSync(password, bcrypt.genSaltSync(10));
      if (!profile_id) {
        return reject({ message: "Profile Id must NOT be empty!" });
      }

      let result = await models.movies.create({
        movie_name,
        rating,
        cast,
        genre,
        release_date,
        is_active: true,
        created_by: profile_id,
        updated_by: profile_id,
      });

      resolve(result);
    } catch (err) {
      reject(err);
    }
  });
};

export const Update = ({
  id,
  profile_id,
  movie_name,
  rating,
  cast,
  genre,
  release_date,
}) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (!profile_id) {
        return reject({ message: "Profile Id must NOT be empty!" });
      }

      // password = bcrypt.hashSync(password, bcrypt.genSaltSync(10));
      if (!id) {
        return reject({ message: "Movie ID must NOT be empty" });
      }

      let result = await models.movies.update(
        {
          movie_name,
          rating,
          cast,
          genre,
          release_date,
          updated_at: new Date(),
          updated_bt: profile_id,
        },
        {
          where: {
            id,
            created_by: profile_id,
            is_active: true,
          },
        }
      );

      resolve(result);
    } catch (err) {
      reject(err);
    }
  });
};

export const Delete = ({ profile_id, id }) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (!profile_id) {
        return reject({ message: "Profile Id must NOT be empty!" });
      }

      // password = bcrypt.hashSync(password, bcrypt.genSaltSync(10));
      if (!id) {
        return reject({ message: "ID must NOT be empty" });
      }

      let result = await models.movies.update(
        {
          is_active: false,
          updated_at: new Date(),
          updated_by: profile_id,
        },
        {
          where: {
            id,
            created_by: profile_id,
            is_active: true,
          },
        }
      );

      if (result?.[0] == 0) {
        return resolve({ message: "Failed! Please check the movie id!" });
      }

      resolve({ message: "Movie deleted successfully!" });
    } catch (err) {
      reject(err);
    }
  });
};

export const GetAll = (params) => {
  return new Promise(async (resolve, reject) => {
    try {
      let { profile_id, start = 0, length = 100 } = params;

      if (!profile_id) {
        return reject({ message: "Profile Id must NOT be empty!" });
      }

      let where = {
        created_by: profile_id,
        is_active: true,
      };
      let searchText = params["search[value]"];
      if (searchText) {
        where[Op.or] = [
          { movie_name: { [Op.iLike]: `%${searchText}%` } },
          { genre: { [Op.iLike]: `%${searchText}%` } },
          models.sequelize.where(
            models.sequelize.cast(
              models.sequelize.col("release_date"),
              "varchar"
            ),
            {
              [Op.iLike]: `%${searchText}%`,
            }
          ),
          models.sequelize.where(
            models.sequelize.cast(models.sequelize.col("rating"), "varchar"),
            {
              [Op.iLike]: `%${searchText}%`,
            }
          ),
          models.sequelize.where(
            models.sequelize.cast(models.sequelize.col("cast"), "varchar"),
            {
              [Op.iLike]: `%${searchText}%`,
            }
          ),
        ];
      }

      let result = await models.movies.findAndCountAll({
        where,
        limit: length,
        offset: start,
        order: [["updated_at", "DESC"]],
        raw: true,
      });
      resolve(result);
    } catch (err) {
      reject(err);
    }
  });
};

export const CountByMovieName = ({ profile_id, movie_name }) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (!profile_id) {
        return reject({ message: "Profile Id must NOT be empty!" });
      }

      if (!movie_name) {
        return reject({ message: "Movie Name must NOT be empty!" });
      }

      let result = await models.movies.count({
        where: {
          [Op.and]: models.sequelize.where(
            models.sequelize.fn("lower", models.sequelize.col("movie_name")),
            movie_name?.toLowerCase()
          ),
          created_by: profile_id,
          is_active: true,
        },
        raw: true,
      });
      resolve(result);
    } catch (err) {
      reject(err);
    }
  });
};

export const GetAllNames = ({ profile_id }) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (!profile_id) {
        return reject({ message: "Profile Id must NOT be empty!" });
      }

      let result = await models.movies.findAll({
        attributes: ["id", ["movie_name", "text"]],
        where: {
          created_by: profile_id,
          is_active: true,
        },
        order: [["updated_at", "DESC"]],
        raw: true,
      });
      resolve(result);
    } catch (err) {
      reject(err);
    }
  });
};

export const Get = ({ profile_id, movie_id }) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (!profile_id) {
        return reject({ message: "Profile Id must NOT be empty!" });
      }

      if (!movie_id || movie_id == "null") {
        return reject({ message: "Movie ID must NOT be empty!" });
      }

      let result = await models.movies.findOne({
        where: {
          id: movie_id,
          created_by: profile_id,
          is_active: true,
        },
        raw: true,
      });
      resolve(result);
    } catch (err) {
      reject(err);
    }
  });
};

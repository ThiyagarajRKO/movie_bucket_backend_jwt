import models from "../../models";

export const Insert = ({ user_id, first_name }) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (!user_id) return reject({ message: "User id must NOT be empty" });

      if (!first_name)
        return reject({ message: "FirstName must NOT be empty" });

      let result = await models.user_profiles.create({
        user_id,
        first_name,
        is_active: true,
        created_by: user_id,
        updated_by: user_id,
      });

      resolve(result);
    } catch (err) {
      reject(err);
    }
  });
};

export const CountById = ({ id }) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (!id) {
        return reject({ message: "Email Id must NOT be empty!" });
      }

      let result = await models.user_profiles.count({
        where: {
          id,
          is_active: true,
        },
        raw: true,
      });
      resolve(result);
    } catch (err) {
      reject(err);
    }
  });
};

export const GetByUserId = ({ user_id }) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (!user_id) {
        return reject({ message: "User Id must NOT be empty!" });
      }

      let result = await models.user_profiles.findOne({
        where: {
          user_id,
          is_active: true,
        },
        raw: true,
      });
      resolve(result);
    } catch (err) {
      reject(err);
    }
  });
};

export const Get = ({ id }) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (!id) {
        return reject({ message: "Id must NOT be empty!" });
      }

      let result = await models.user_profiles.findOne({
        where: {
          id,
          is_active: true,
        },
        raw: true,
      });
      resolve(result);
    } catch (err) {
      reject(err);
    }
  });
};

# Movies Bucket

Platform that the users can store their favourite movie list

## Technologies

Configured some security tools as well to prevent from some the attacks

`Rate limiter`

To prevent from brute force attack configured this tool. It allows 8 API request per IP for 1 second

`Template Engine`

To render the webpages configured ejs in nodejs

`JWT`

configured this tool for stateless user management

`DB Diagram`
https://dbdiagram.io/d/636b7eb5c9abfc61117156b5

`Postman API collection`
https://documenter.getpostman.com/view/8448250/2s8YeixG7t

"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      { tableName: "users", schema: "auth" },
      {
        id: {
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.UUIDV4,
        },
        mobile: {
          type: Sequelize.STRING,
        },
        email_id: {
          type: Sequelize.STRING,
          allowNull: false,
          unique: true,
        },
        password: {
          type: Sequelize.STRING,
        },
        is_active: {
          type: Sequelize.BOOLEAN,
          defaultValue: false,
        },
        created_at: {
          defaultValue: new Date().toISOString(),
          type: Sequelize.DATE,
        },
        updated_at: {
          defaultValue: new Date().toISOString(),
          type: Sequelize.DATE,
        },
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable({ tableName: "users", schema: "auth" });
  },
};
